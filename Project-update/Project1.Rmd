---
title: "Project 1"
output: html_document
---

```{r}
library(devtools)
library(remotes)
suppressPackageStartupMessages({library("maEndToEnd")})
   library(Biobase)
    library(oligoClasses)
    library(ArrayExpress)
    library(pd.hugene.1.0.st.v1)
    library(hugene10sttranscriptcluster.db)
    library(oligo)
    library(arrayQualityMetrics)
    library(limma)
    library(topGO)
    library(ReactomePA)
    library(clusterProfiler)
    library(gplots)
    library(ggplot2)
    library(geneplotter)
    library(RColorBrewer)
    library(pheatmap)
    library(dplyr)
    library(tidyr)
    library(stringr)
    library(matrixStats)
    library(genefilter)
    library(openxlsx)
```

```{r}
raw_data_dir <- "C:/Users/ooo/Desktop/k/Uni/Fall 2020/Functional Genomics/Project 1"

if (!dir.exists(raw_data_dir)) {
    dir.create(raw_data_dir)
}

```

```{r}
sdrf_location <- file.path(raw_data_dir, "E-MTAB-6366.sdrf.txt")
SDRF <- read.delim(sdrf_location)

rownames(SDRF) <- SDRF$Array.Data.File
SDRF <- AnnotatedDataFrame(SDRF)
```


```{r}
raw_data <- oligo::read.celfiles(filenames = file.path(raw_data_dir, 
                                                SDRF$Array.Data.File),
                                    verbose = FALSE, phenoData = SDRF)
stopifnot(validObject(raw_data))
```

```{r}
head(Biobase::pData(raw_data))
```
```{r}
Biobase::pData(raw_data)[7, 30]="wild type phenotype HE4"
Biobase::pData(raw_data)[8, 30]="wild type phenotype HE4"
Biobase::pData(raw_data)[9, 30]="wild type phenotype HE4"
```

```{r}
Biobase::pData(raw_data) <- Biobase::pData(raw_data)[, c("Source.Name", "Assay.Name",
                                                         "Factor.Value.phenotype.")]
```

```{r}
Biobase::exprs(raw_data)[1:5, 1:5]
```

```{r}
exp_raw <- log2(Biobase::exprs(raw_data))
PCA_raw <- prcomp(t(exp_raw), scale. = FALSE)

percentVar <- round(100*PCA_raw$sdev^2/sum(PCA_raw$sdev^2),1)
sd_ratio <- sqrt(percentVar[2] / percentVar[1])

dataGG <- data.frame(PC1 = PCA_raw$x[,1], PC2 = PCA_raw$x[,2],
                   Phenotype = pData(raw_data)$Factor.Value.phenotype.,
                    Individual = pData(raw_data)$Source.Name)

ggplot(dataGG, aes(PC1, PC2)) +
      geom_point(aes(colour = Phenotype)) +
  ggtitle("PCA plot of the log-transformed raw expression data") +
  xlab(paste0("PC1, VarExp: ", percentVar[1], "%")) +
  ylab(paste0("PC2, VarExp: ", percentVar[2], "%")) +
  theme(plot.title = element_text(hjust = 0.5))+
  coord_fixed(ratio = sd_ratio) +
  scale_shape_manual(values = c(4,15)) + 
  scale_color_manual(values = c("#058ED9", "#F17300", "#EC368D", "#071013"))
```

```{r}
oligo::boxplot(raw_data, target = "core", 
               main = "Boxplot of log2-intensitites for the raw data")
```

```{r}
arrayQualityMetrics(expressionset = raw_data,
  outdir = "C:/Users/ooo/Desktop/k/Uni/Fall 2020/Functional Genomics/Project 1",
   force = TRUE, do.logtransform = TRUE,
    intgroup = c("Factor.Value.phenotype."))
```

```{r}
head(ls("package:hugene10sttranscriptcluster.db"))
```

```{r}
palmieri_eset <- oligo::rma(raw_data, target = "core", normalize = FALSE)
```

```{r}
row_medians_assayData <- 
  Biobase::rowMedians(as.matrix(Biobase::exprs(palmieri_eset)))

RLE_data <- sweep(Biobase::exprs(palmieri_eset), 1, row_medians_assayData)

RLE_data <- as.data.frame(RLE_data)
RLE_data_gathered <- 
  tidyr::gather(RLE_data, patient_array, log2_expression_deviation)

ggplot2::ggplot(RLE_data_gathered, aes(patient_array,
                                       log2_expression_deviation)) + 
  geom_boxplot(outlier.shape = NA) + 
  ylim(c(-2, 2)) + 
  theme(axis.text.x = element_text(colour = "aquamarine4", 
                                  angle = 60, size = 6.5, hjust = 1 ,
                                  face = "bold"))
```

```{r}
palmieri_eset_norm <- oligo::rma(raw_data, target = "core")
```

```{r}
exp_palmieri <- Biobase::exprs(palmieri_eset_norm)
PCA <- prcomp(t(exp_palmieri), scale = FALSE)

percentVar <- round(100*PCA$sdev^2/sum(PCA$sdev^2),1)
sd_ratio <- sqrt(percentVar[2] / percentVar[1])

dataGG <- data.frame(PC1 = PCA_raw$x[,1], PC2 = PCA_raw$x[,2],
                     Phenotype = pData(raw_data)$Factor.Value.phenotype.,
                    Individual = pData(raw_data)$Source.Name)


ggplot(dataGG, aes(PC1, PC2)) +
      geom_point(aes(colour = Phenotype)) +
  ggtitle("PCA plot of the calibrated, summarized data") +
  xlab(paste0("PC1, VarExp: ", percentVar[1], "%")) +
  ylab(paste0("PC2, VarExp: ", percentVar[2], "%")) +
  theme(plot.title = element_text(hjust = 0.5)) +
  coord_fixed(ratio = sd_ratio) +
  scale_shape_manual(values = c(4,15)) + 
  scale_color_manual(values = c("#058ED9", "#F17300", "#EC368D", "#071013"))
```

```{r}
phenotype_names <- ifelse(str_detect(pData
                                    (palmieri_eset_norm)$Factor.Value.phenotype.,
                             "HE4 overexpression"), "OVCAR8-C5", "OVCAR8-NV")

annotation_for_heatmap <- 
  data.frame(Phenotype = phenotype_names)

row.names(annotation_for_heatmap) <- row.names(pData(palmieri_eset_norm))


```

```{r}

```

