---
title: "Project 1"
output: html_document
---

```{r}
library(devtools)
library(remotes)
suppressPackageStartupMessages({library("maEndToEnd")})
   library(Biobase)
    library(oligoClasses)
    library(ArrayExpress)
    library(pd.hugene.1.0.st.v1)
    library(hugene10sttranscriptcluster.db)
    library(oligo)
    library(arrayQualityMetrics)
    library(limma)
    library(topGO)
    library(ReactomePA)
    library(clusterProfiler)
    library(gplots)
    library(ggplot2)
    library(geneplotter)
    library(RColorBrewer)
    library(pheatmap)
    library(dplyr)
    library(tidyr)
    library(stringr)
    library(matrixStats)
    library(genefilter)
    library(openxlsx)
```

```{r}
raw_data_dir <- "C:/Users/ooo/Desktop/k/Uni/Fall 2020/Functional Genomics/Project 1"

if (!dir.exists(raw_data_dir)) {
    dir.create(raw_data_dir)
}

```

```{r}
sdrf_location <- file.path(raw_data_dir, "E-MTAB-6366.sdrf.txt")
SDRF <- read.delim(sdrf_location)

rownames(SDRF) <- SDRF$Array.Data.File
SDRF <- AnnotatedDataFrame(SDRF)
```


```{r}
raw_data <- oligo::read.celfiles(filenames = file.path(raw_data_dir, 
                                                SDRF$Array.Data.File),
                                    verbose = FALSE, phenoData = SDRF)
stopifnot(validObject(raw_data))
```

```{r}
head(Biobase::pData(raw_data))
```

Since the column Assay.Name doesnt not differentiate between untreated wild type phenotype and wild type phenotype treated with rHE4,the code will take them as 3 groups instead of 4:
```{r}
Biobase::pData(raw_data)[,30]
```
Here, only the last 3 are untreated wild type phenotype, whereas the 3 before them were treated with rHE4, so we edit them such that there's a difference between the 2 groups:
```{r}
Biobase::pData(raw_data)[7, 30]="wild type phenotype HE4"
Biobase::pData(raw_data)[8, 30]="wild type phenotype HE4"
Biobase::pData(raw_data)[9, 30]="wild type phenotype HE4"
```

The columns of interest for us are the following:

* identifiers of the individuals, i.e. columns “Source.Name”, “Assay.Name”
* cell experiment type, i.e. “Factor.Value.phenotype.”
We now subselect the corresponding columns:
```{r}
Biobase::pData(raw_data) <- Biobase::pData(raw_data)[, c("Source.Name", "Assay.Name",
                                                         "Factor.Value.phenotype.")]
```

## Quality control of the raw data

Here we check for outliers and try to see whether the data clusters as expected, e.g. by the experimental conditions. The rows represent the microarray probes, i.e. the single DNA locations on the chip, while the columns represent one microarray, i.e. a sample of inflamed and non-inflamed tissue of every patient, respectively.
```{r}
Biobase::exprs(raw_data)[1:5, 1:5]
```

For quality control, we take the log2 of Biobase::exprs(raw_data), as expression data is commonly analyzed on a logarithmic scale.
We then perform 2 principal component analysis (PCA) and plot them, the first for OVCAR8-NV vs OVCAR8-C5, and the second for OVCAR8-WT vs OVCAR8-WT-rHE4 samples.

```{r}
exp_raw <- log2(Biobase::exprs(raw_data))
PCA_raw <- prcomp(t(exp_raw), scale. = FALSE)

percentVar <- round(100*PCA_raw$sdev^2/sum(PCA_raw$sdev^2),1)
sd_ratio <- sqrt(percentVar[2] / percentVar[1])

dataGG <- data.frame(PC1 = PCA_raw$x[,1], PC2 = PCA_raw$x[,2],
                   Phenotype = pData(raw_data)$Factor.Value.phenotype.,
                    Individual = pData(raw_data)$Source.Name)

ggplot(dataGG[1:6,], aes(PC1, PC2)) +
      geom_point(aes(colour = Phenotype)) +
  ggtitle("PCA plot of the log-transformed raw expression data
          for OVCAR8-NV and OVCAR8-C5") +
  xlab(paste0("PC1, VarExp: ", percentVar[1], "%")) +
  ylab(paste0("PC2, VarExp: ", percentVar[2], "%")) +
  theme(plot.title = element_text(hjust = 0.5))+
  coord_fixed(ratio = sd_ratio) +
  scale_shape_manual(values = c(4,15)) + 
  scale_color_manual(values = c("#058ED9", "#F17300"))
```

```{r}
percentVar <- round(100*PCA_raw$sdev^2/sum(PCA_raw$sdev^2),1)
sd_ratio <- sqrt(percentVar[2] / percentVar[1])

dataGG <- data.frame(PC1 = PCA_raw$x[,1], PC2 = PCA_raw$x[,2],
                   Phenotype = pData(raw_data)$Factor.Value.phenotype.,
                    Individual = pData(raw_data)$Source.Name)

ggplot(dataGG[7:12,], aes(PC1, PC2)) +
      geom_point(aes(colour = Phenotype)) +
  ggtitle("PCA plot of the log-transformed raw expression data
          for OVCAR8-WT and OVCAR8-WT-rHE4") +
  xlab(paste0("PC1, VarExp: ", percentVar[1], "%")) +
  ylab(paste0("PC2, VarExp: ", percentVar[2], "%")) +
  theme(plot.title = element_text(hjust = 0.5))+
  coord_fixed(ratio = sd_ratio) +
  scale_shape_manual(values = c(4,15)) + 
  scale_color_manual(values = c("#EC368D", "#071013"))
```
In both graphs, there are no clear clusters.


We also represent the probe intensities via a boxplot graph with one box per individual microarray:
```{r}
oligo::boxplot(raw_data, target = "core", 
               main = "Boxplot of log2-intensitites for the raw data")
```

We now produce an html report, containing the quality control plots together with a description of their aims and an identification of possible outliers.
```{r}
arrayQualityMetrics(expressionset = raw_data,
  outdir = "C:/Users/ooo/Desktop/k/Uni/Fall 2020/Functional Genomics/Project 1",
   force = TRUE, do.logtransform = TRUE,
    intgroup = c("Factor.Value.phenotype."))
```

## Background adjustment, calibration, summarization and annotation


Before calibrating and evaluating the data, we want to perform another quality control procedure, namely Relative Log Expression (RLE), as described in the article by Gandolfo et al (9). To this end, we first perform an RMA without prior normalization:
```{r}
palmieri_eset <- oligo::rma(raw_data, target = "core", normalize = FALSE)
```

The RLE is performed by calculating the median log2 intensity of every transcript across all arrays.
We do this by calculating the row medians of exprs(palmieri_eset), as the transcripts are represented by the rows and the single microarrays by the columns.
Note that we do not have to apply the log2 manually, as the output data of the RMA function is in log2 scale by default.
We then substract this transcript median intensity from every transcript intensity via the sweep function.
```{r}
row_medians_assayData <- 
  Biobase::rowMedians(as.matrix(Biobase::exprs(palmieri_eset)))

RLE_data <- sweep(Biobase::exprs(palmieri_eset), 1, row_medians_assayData)

RLE_data <- as.data.frame(RLE_data)
RLE_data_gathered <- 
  tidyr::gather(RLE_data, patient_array, log2_expression_deviation)

ggplot2::ggplot(RLE_data_gathered, aes(patient_array,
                                       log2_expression_deviation)) + 
  geom_boxplot(outlier.shape = NA) + 
  ylim(c(-2, 2)) + 
  theme(axis.text.x = element_text(colour = "aquamarine4", 
                                  angle = 60, size = 6.5, hjust = 1 ,
                                  face = "bold"))
```
Note that the y-axis now displays for each microarray the deviation of expression intensity from the median expression of the respective single transcripts across arrays.

Boxes with a larger extension therefore indicate an unusually high deviation from the median in a lot of transcripts, suggesting that these arrays are different from most of the others in some way.

Boxes that are shifted in y-direction indicate a systematically higher or lower expression of the majority of transcripts in comparison to most of the other arrays. This could be caused by quality issues or batch effects.


## RMA calibration of the data

First step of RMA is the background normalization of the data:
```{r}
library(preprocessCore)
bg_corrected_data=rma.background.correct(exprs(raw_data))
```

```{r}
  
  # For ranking, sorting, accessing and manipulating rows and columns:
  # We focus on using apply() family methods instead of for loops to optimize speed
  
  # ranked cols matrix: Contains Expressions-> Ranks in their matrix
  ranked_cols_matrix <- apply(bg_corrected_data,2, rank,ties.method="min")
  
  # we can use apply(data,2,sort) to get the sorted matrix by columns
  sorted_cols_matrix <- apply(bg_corrected_data, 2, sort)

  # Get the mean of every row in the sorted_cols_matrix
  mean_by_row <- rowMeans(sorted_cols_matrix)
  
 
  replace_w_means <- function(column, means){
    # Rank-Frequency table: Gets all repeated expressions and their frequency
    rank_freq <- data.frame(table(column))
    rank_freq <- as.matrix(rank_freq)
    
    # Function to update row means into row means but column specific, check below for more...
    update_mean <- function(rf, med){
    new_mean <- rep(mean(med[as.integer(rf[1]):(as.integer(rf[1])+as.integer(rf[2])-1)]),rf[2])
    return (new_mean)
    }
    # All Repeated Ranks will influence the means => replace the means with average of means
    new_means <- apply(rank_freq,1, update_mean, med=mean_by_row)
    new_means <- unlist(new_means)
    
    # Index to mean function: Replace all values with rank x with average mean of the same rank
    index_to_mean <- function(my_index, my_mean){
    return(my_mean[my_index])
    }
    # Apply index_to_mean on column X
    final_result <- lapply(column, index_to_mean, my_mean=new_means)
    final_result <- unlist(final_result)
  
    return(final_result)
}
 # Apply replace_w_means to every column of the matrix:
  quantile_normalized  <- apply(ranked_cols_matrix,2,replace_w_means, means=mean_by_row)
 
```

```{r}
head(normalize.quantiles(bg_corrected_data))-head(quantile_normalized)
```


```{r}
palmieri_eset_norm <- oligo::rma(raw_data, target = "core")
```

```{r}
exp_palmieri <- Biobase::exprs(palmieri_eset_norm)
PCA <- prcomp(t(exp_palmieri), scale = FALSE)

percentVar <- round(100*PCA$sdev^2/sum(PCA$sdev^2),1)
sd_ratio <- sqrt(percentVar[2] / percentVar[1])

dataGG <- data.frame(PC1 = PCA_raw$x[,1], PC2 = PCA_raw$x[,2],
                     Phenotype = pData(raw_data)$Factor.Value.phenotype.,
                    Individual = pData(raw_data)$Source.Name)


ggplot(dataGG[1:6,], aes(PC1, PC2)) +
      geom_point(aes(colour = Phenotype)) +
  ggtitle("PCA plot of the calibrated, summarized data") +
  xlab(paste0("PC1, VarExp: ", percentVar[1], "%")) +
  ylab(paste0("PC2, VarExp: ", percentVar[2], "%")) +
  theme(plot.title = element_text(hjust = 0.5)) +
  coord_fixed(ratio = sd_ratio) +
  scale_shape_manual(values = c(4,15)) + 
  scale_color_manual(values = c("#058ED9", "#F17300", "#EC368D", "#071013"))
```

```{r}
exp_palmieri <- Biobase::exprs(palmieri_eset_norm)
PCA <- prcomp(t(exp_palmieri), scale = FALSE)

percentVar <- round(100*PCA$sdev^2/sum(PCA$sdev^2),1)
sd_ratio <- sqrt(percentVar[2] / percentVar[1])

dataGG <- data.frame(PC1 = PCA_raw$x[,1], PC2 = PCA_raw$x[,2],
                     Phenotype = pData(raw_data)$Factor.Value.phenotype.,
                    Individual = pData(raw_data)$Source.Name)


ggplot(dataGG[7:12,], aes(PC1, PC2)) +
      geom_point(aes(colour = Phenotype)) +
  ggtitle("PCA plot of the calibrated, summarized data") +
  xlab(paste0("PC1, VarExp: ", percentVar[1], "%")) +
  ylab(paste0("PC2, VarExp: ", percentVar[2], "%")) +
  theme(plot.title = element_text(hjust = 0.5)) +
  coord_fixed(ratio = sd_ratio) +
  scale_shape_manual(values = c(4,15)) + 
  scale_color_manual(values = c("#EC368D", "#071013"))
```

```{r}
phenotype_names <- ifelse(str_detect(pData
                                    (palmieri_eset_norm)$Factor.Value.phenotype.[1:6],
                             "HE4 overexpression"), "OVCAR8-C5", "OVCAR8-NV")

annotation_for_heatmap <- 
  data.frame(Phenotype = phenotype_names)

row.names(annotation_for_heatmap) <- row.names(pData(palmieri_eset_norm)[1:6,])


```

 ifelse(str_detect(pData
                                    (palmieri_eset_norm)$Factor.Value.phenotype.,
                             "empty vector"), "OVCAR8-NV", ifelse(str_detect(pData
                                    (palmieri_eset_norm)$Factor.Value.phenotype.,
                             "wild type phenotype HE4"), "OVCAR8-WT-rHE4", "OVCAR8-WT")))
                             
                             
                             
                             , 'OVCAR8-WT-rHE4' = "#EC368D", 'OVCAR8-WT' = "green3"


```{r}
dists <- as.matrix(dist(t(exp_palmieri), method = "manhattan"))

rownames(dists) <- row.names(pData(palmieri_eset_norm))
hmcol <- rev(colorRampPalette(RColorBrewer::brewer.pal(9, "YlOrRd"))(255))
colnames(dists) <- NULL
diag(dists) <- NA

ann_colors <- list(
  Phenotype = c('OVCAR8-C5' = "blue", 'OVCAR8-NV' = "#F17300")
                   )
pheatmap(dists[1:6,1:6], col = (hmcol), 
         annotation_row = annotation_for_heatmap,
         annotation_colors = ann_colors,
         legend = TRUE, 
         treeheight_row = 0,
         legend_breaks = c(min(dists[1:6,1:6], na.rm = TRUE), 
                         max(dists[1:6,1:6], na.rm = TRUE)), 
         legend_labels = (c("small distance", "large distance")),
         main = "Clustering heatmap for the calibrated samples")
```
```{r}
phenotype_names <- ifelse(str_detect(pData
                                    (palmieri_eset_norm)$Factor.Value.phenotype.[7:12],
                             "wild type phenotype HE4"), "OVCAR8-WT-rHE4", "OVCAR8-WT")

annotation_for_heatmap <- 
  data.frame(Phenotype = phenotype_names)

row.names(annotation_for_heatmap) <- row.names(pData(palmieri_eset_norm)[7:12,])


```


```{r}
dists <- as.matrix(dist(t(exp_palmieri), method = "manhattan"))

rownames(dists) <- row.names(pData(palmieri_eset_norm))
hmcol <- rev(colorRampPalette(RColorBrewer::brewer.pal(9, "YlOrRd"))(255))
colnames(dists) <- NULL
diag(dists) <- NA

ann_colors <- list(
  Phenotype = c('OVCAR8-WT-rHE4' = "#EC368D", 'OVCAR8-WT' = "green3")
                   )
pheatmap(dists[7:12,7:12], col = (hmcol), 
         annotation_row = annotation_for_heatmap,
         annotation_colors = ann_colors,
         legend = TRUE, 
         treeheight_row = 0,
         legend_breaks = c(min(dists[7:12,7:12], na.rm = TRUE), 
                         max(dists[7:12,7:12], na.rm = TRUE)), 
         legend_labels = (c("small distance", "large distance")),
         main = "Clustering heatmap for the calibrated samples")
```


```{r}
palmieri_medians <- rowMedians(Biobase::exprs(palmieri_eset_norm))

hist_res <- hist(palmieri_medians, 100, col = "cornsilk1", freq = FALSE, 
            main = "Histogram of the median intensities", 
            border = "antiquewhite4",
            xlab = "Median intensities")
```


```{r}
man_threshold <- 2.5

hist_res <- hist(palmieri_medians, 100, col = "cornsilk", freq = FALSE, 
            main = "Histogram of the median intensities",
            border = "antiquewhite4",
            xlab = "Median intensities")

abline(v = man_threshold, col = "coral4", lwd = 2)
```

```{r}
no_of_samples <- 
  table(paste0(pData(palmieri_eset_norm)$Factor.Value.phenotype.))
no_of_samples 
```

```{r}
samples_cutoff <- min(no_of_samples)

idx_man_threshold <- apply(Biobase::exprs(palmieri_eset_norm), 1,
                           function(x){
                          sum(x > man_threshold) >= samples_cutoff})
                          table(idx_man_threshold)
```

```{r}
palmieri_manfiltered <- subset(palmieri_eset_norm, idx_man_threshold)
```


## Annotation of the transcript clusters

```{r}
anno_palmieri <- AnnotationDbi::select(hugene10sttranscriptcluster.db,
                                  keys = (featureNames(palmieri_manfiltered)),
                                  columns = c("SYMBOL", "GENENAME"),
                                  keytype = "PROBEID")

anno_palmieri <- subset(anno_palmieri, !is.na(SYMBOL))
```

```{r}
anno_grouped <- group_by(anno_palmieri, PROBEID)
anno_summarized <- 
  dplyr::summarize(anno_grouped, no_of_matches = n_distinct(SYMBOL))

head(anno_summarized)
```

```{r}
anno_filtered <- filter(anno_summarized, no_of_matches > 1)

head(anno_filtered)
```

```{r}
probe_stats <- anno_filtered 

nrow(probe_stats)
```

```{r}
ids_to_exlude <- (featureNames(palmieri_manfiltered) %in% probe_stats$PROBEID)

table(ids_to_exlude)
```

```{r}
palmieri_final <- subset(palmieri_manfiltered, !ids_to_exlude)

validObject(palmieri_final)
```

```{r}
head(anno_palmieri)
```

```{r}
fData(palmieri_final)$PROBEID <- rownames(fData(palmieri_final))
```

```{r}
fData(palmieri_final) <- left_join(fData(palmieri_final), anno_palmieri)
```

```{r}
# restore rownames after left_join
rownames(fData(palmieri_final)) <- fData(palmieri_final)$PROBEID 
    
validObject(palmieri_final)
```

```{r}
individual <- 
  as.character(Biobase::pData(palmieri_final)$Assay.Name[1:6])

type <- str_replace_all(Biobase::pData(palmieri_final)$Factor.Value.phenotype.[1:6],
                  " ", "_")

type <- ifelse(type == "HE4_overexpression",
                 "C5", "NV")
```



```{r}
individual_2 <- 
  as.character(Biobase::pData(palmieri_final)$Assay.Name[7:12])

type_2 <- str_replace_all(Biobase::pData(palmieri_final)$Factor.Value.phenotype.[7:12],
                  " ", "_")

type_2 <- ifelse(type_2 == "wild_type_phenotype_HE4",
                 "WT-HE", "WT")
```

noppp
```{r}
i_C5 <- individual
design_palmieri_C5 <- model.matrix(~ 0 + type[1:6] + i_C5)
colnames(design_palmieri_C5)[1:2] <- c("C5", "NV")
rownames(design_palmieri_C5) <- individual

i_WT <- individual_2
type_2=factor(type_2)
design_palmieri_WT <- model.matrix(~ 0 + type_2)
colnames(design_palmieri_WT)[1:2] <- c("WT", "WT-HE")
rownames(design_palmieri_WT) <- individual_2
```


```{r}
crat_expr <- Biobase::exprs(palmieri_final)["7952522", 1:6]
crat_data <- as.data.frame(crat_expr)
colnames(crat_data)[1] <- "org_value"
crat_data <- mutate(crat_data, i_C5, type)

crat_data$type <- factor(crat_data$type, levels = c("C5", "NV"))

crat_coef <- lmFit(palmieri_final[,1:6], design = design_palmieri_C5)$coefficients["7952522",]

View(crat_coef)

```
hhhhhhhhhhhhhhhhh
```{r}
i_C5 <- individual
type=factor(type)
design_palmieri_C5 <- model.matrix(~ 0 + type)
colnames(design_palmieri_C5)[1:2] <- c("C5", "NV")
rownames(design_palmieri_C5) <- individual

i_WT <- individual
type_2=factor(type_2)
design_palmieri_WT <- model.matrix(~ 0 + type_2)
colnames(design_palmieri_WT)[1:2] <- c("WT", "WT_HE")
rownames(design_palmieri_WT) <- individual_2
```


```{r}
crat_expr <- Biobase::exprs(palmieri_final)["8108697",1:6]
crat_data <- as.data.frame(crat_expr)
colnames(crat_data)[1] <- "org_value"
crat_data <- mutate(crat_data, type)

crat_data$type <- factor(crat_data$type, levels = c("C5", "NV"))

crat_coef <- lmFit(palmieri_final[,1:6], design = design_palmieri_C5)$coefficients["8108697",]

crat_coef

```
hhhhhhhhhhhhhhhhhhhhhhh

```{r}
crat_fitted <- design_palmieri_C5 %*% crat_coef
rownames(crat_fitted) <- names(crat_expr)
colnames(crat_fitted) <- "fitted_value"

crat_fitted
```

```{r}
crat_C5 <- na.exclude(crat_data$org_value[type == "C5"])
crat_NV <- na.exclude(crat_data$org_value[type == "NV"])
res_t <- t.test(crat_C5 ,crat_NV , paired = FALSE)
res_t
```

```{r}
contrast_matrix_C5 <- makeContrasts(NV-C5, levels = design_palmieri_C5)

palmieri_fit_C5 <- eBayes(contrasts.fit(lmFit(palmieri_final[,1:6],
                                design = design_palmieri_C5),
                                contrast_matrix_C5))

contrast_matrix_WT <- makeContrasts(WT_HE-WT, levels = design_palmieri_WT)

palmieri_fit_WT <- eBayes(contrasts.fit(lmFit(palmieri_final[,7:12],
                                design = design_palmieri_WT),
                                contrast_matrix_WT))
```

```{r}
table_C5 <- topTable(palmieri_fit_C5, number = Inf)
#table_C5=na.exclude(table_C5)
head(table_C5)
```

```{r}
hist(table_C5$P.Value, col = brewer.pal(3, name = "Set2")[1],
     main = "C5 vs NV", xlab = "p-values")
```


```{r}
table_WT <- topTable(palmieri_fit_WT, number = Inf)
#table_WT=na.exclude(table_WT)
head(table_WT)
```

```{r}
hist(table_WT$P.Value, col = brewer.pal(3, name = "Set2")[2],
     main = "WT vs WT-HE", xlab = "p-values")
```

```{r}
nrow(subset(table_C5, P.Value < 0.001))
```

```{r}
#fpath <- system.file( "C:/Users/ooo/Desktop/k/Uni/Fall 2020/Functional Genomics/Project 1/data_sheet_1.xlsx", package = "maEndToEnd", mustWork = TRUE)
palmieri_DE_res <- sapply(1:1, function(i) read.xlsx(cols = 4, "C:/Users/ooo/Desktop/k/Uni/Fall 2020/Functional Genomics/Project 1/data_sheet_1.xlsx", 
                                                     sheet = i, startRow = 1))

names(palmieri_DE_res) <- c("C5-NV")
palmieri_DE_res <- lapply(palmieri_DE_res, as.character)
#paper_DE_genes_CD <- Reduce("c", palmieri_DE_res[1:2])
#paper_DE_genes_UC <- Reduce("c", palmieri_DE_res[3:4])

overlap_CD <- length(intersect(subset(table_C5, P.Value < 0.001)$SYMBOL,  
                               palmieri_DE_res)) / length(palmieri_DE_res)


#overlap_UC <- length(intersect(subset(table_UC, P.Value < 0.001)$SYMBOL,
 #                              paper_DE_genes_UC)) / length(paper_DE_genes_UC)
overlap_CD
```
```{r}
#h=palmieri_final[,1:3]/palmieri_final[,4:6]
```

```{r}
DE_genes_C5 <- subset(table_C5, adj.P.Val < 0.1)$PROBEID
```

```{r}
back_genes_idx <- genefilter::genefinder(palmieri_final, 
                                        as.character(DE_genes_C5), 
                                        method = "manhattan", scale = "none")
```

```{r}
back_genes_idx <- sapply(back_genes_idx, function(x)x$indices)
```

```{r}
back_genes <- featureNames(palmieri_final)[back_genes_idx]
back_genes <- setdiff(back_genes, DE_genes_C5)

    
intersect(back_genes, DE_genes_C5)
```

```{r}
length(back_genes)
```

```{r}
multidensity(list(
        all = table_C5[,"AveExpr"] ,
        fore = table_C5[DE_genes_C5 , "AveExpr"],
        back = table_C5[rownames(table_C5) %in% back_genes, "AveExpr"]),
        col = c("#e46981", "#ae7ee2", "#a7ad4a"),
     xlab = "mean expression",
   main = "DE genes for C5/NV-background-matching")
```

```{r}
gene_IDs <- rownames(table_C5)
in_universe <- gene_IDs %in% c(DE_genes_C5, back_genes)
in_selection <- gene_IDs %in% DE_genes_C5

all_genes <- in_selection[in_universe]
all_genes <- factor(as.integer(in_selection[in_universe]))
names(all_genes) <- gene_IDs[in_universe] 
```

```{r}
top_GO_data <- new("topGOdata", ontology = "BP", allGenes = all_genes,
 nodeSize = 10, annot = annFUN.db, affyLib = "hugene10sttranscriptcluster.db")
```

```{r}
result_top_GO_elim <- 
  runTest(top_GO_data, algorithm = "elim", statistic = "Fisher")
result_top_GO_classic <- 
  runTest(top_GO_data, algorithm = "classic", statistic = "Fisher")
```

```{r}
res_top_GO <- GenTable(top_GO_data, Fisher.elim = result_top_GO_elim,
        Fisher.classic = result_top_GO_classic,
        orderBy = "Fisher.elim" , topNodes = 100)

genes_top_GO <- printGenes(top_GO_data, whichTerms = res_top_GO$GO.ID,
    chip = "hugene10sttranscriptcluster.db", geneCutOff = 1000)

res_top_GO$sig_genes <- sapply(genes_top_GO, function(x){
                str_c(paste0(x[x$'raw p-value' == 2, "Symbol.id"],";"), 
                      collapse = "")
    })

head(res_top_GO[,1:8], 20)
```

```{r}
showSigOfNodes(top_GO_data, score(result_top_GO_elim), firstSigNodes = 3,
                useInfo = 'def')
```


## A pathway enrichment analysis using reactome

```{r}
entrez_ids <- mapIds(hugene10sttranscriptcluster.db, 
      keys = rownames(table_C5), 
      keytype = "PROBEID",
      column = "ENTREZID")
```

```{r}
reactome_enrich <- enrichPathway(gene = entrez_ids[DE_genes_C5], 
                                universe = entrez_ids[c(DE_genes_C5, 
                                                        back_genes)],
                                organism = "human",
                                pvalueCutoff = 0.05,
                                qvalueCutoff = 0.9, 
                                readable = TRUE)

reactome_enrich@result$Description <- paste0(str_sub(
                                    reactome_enrich@result$Description, 1, 20),
                                    "...")

head(as.data.frame(reactome_enrich@result))[1:6]
```

```{r}
barplot(reactome_enrich)
```

```{r}
#emapplot(reactome_enrich, showCategory = 10)
```

