#!/usr/bin/perl
#!/usr/bin/bash

$file1 = "NIST7035_trimmed_R1_paired.fastq";
$file2 = "NIST7035_trimmed_R1_unpaired.fastq";
$file3 = "NIST7035_trimmed_R2_paired.fastq";
$file4 = "NIST7035_trimmed_R2_unpaired.fastq";



`declare -a R1p`;
`mapfile -t R1p < NIST7035_trimmed_R1_paired.fastq`;

@R1p=`$R1p`;
print"For the paired R1 file:\n\n";
for (my $i=36; $i<102; $i++)
{
	num_avg(\@R1p, $i);
}

@U1p=`$U1p`;
print"For the unpaired R1 file:\n\n";
for (my $i=36; $i<102; $i++)
{
	num_avg(\@U1p, $i);
}

@R2p=`$R2p`;
print"For the paired R2 file:\n\n";
for (my $i=36; $i<102; $i++)
{
	num_avg(\@R2p, $i);
}

@U2p=`$U2p`;
print"For the unpaired R2 file:\n\n";
for (my $i=36; $i<102; $i++)
{
	num_avg(\@U2p, $i);
}

sub num_avg
{
	@array= @{$_[0]};
	$length=@_[1];
		
	$count=0;
	$score=0;

	for ($k=1; $k<@array; $k=$k+4)
	{
		$read = @array[$k];
		chomp $read;
		if ($length==length $read)
		{
			$count++;
			$score = $score + get_score(@array[$k+2]);
		}
	}
	if ($count!=0)
	{$avg = ($score/$count);}
	print "For length = " . $length;
	print "\nThere are " . $count . " reads";
	print "\nAverage scores:" . $avg . "\n\n\n";
}

sub get_score
{
	$ASCII=@_[0];
	@ASCII=split(//, $ASCII);
	$quality=0;
	for ($j=0; $j<@ASCII; $j++)
	{
		if (@ASCII[$j]=="\"")
			{ $quality = $quality + 1; }
			elsif (@ASCII[$j]=="#")
			{ $quality = $quality + 2; }
			elsif (@ASCII[$j]=="\$")
			{ $quality = $quality + 3; }
			elsif (@ASCII[$j]=="%")
			{ $quality = $quality + 4; }
			elsif (@ASCII[$j]=="&")
			{ $quality = $quality + 5; }
			elsif (@ASCII[$j]=="'")
			{ $quality = $quality + 6; }
			elsif (@ASCII[$j]=="(")
			{ $quality = $quality + 7; }
			elsif (@ASCII[$j]==")")
			{ $quality = $quality + 8; }
			elsif (@ASCII[$j]=="*")
			{ $quality = $quality + 9; }
			elsif (@ASCII[$j]=="+")
			{ $quality = $quality + 10; }
			elsif (@ASCII[$j]==",")
			{ $quality = $quality + 11; }
			elsif (@ASCII[$j]=="-")
			{ $quality = $quality + 12; }
			elsif (@ASCII[$j]==".")
			{ $quality = $quality + 13; }
			elsif (@ASCII[$j]=="/")
			{ $quality = $quality + 14; }
			elsif (@ASCII[$j]=="0")
			{ $quality = $quality + 15; }
			elsif (@ASCII[$j]=="1")
			{ $quality = $quality + 16; }
			elsif (@ASCII[$j]=="2")
			{ $quality = $quality + 17; }
			elsif (@ASCII[$j]=="3")
			{ $quality = $quality + 18; }
			elsif (@ASCII[$j]=="4")
			{ $quality = $quality + 19; }
			elsif (@ASCII[$j]=="5")
			{ $quality = $quality + 20; }
			elsif (@ASCII[$j]=="6")
			{ $quality = $quality + 21; }
			elsif (@ASCII[$j]=="7")
			{ $quality = $quality + 22; }
			elsif (@ASCII[$j]=="8")
			{ $quality = $quality + 23; }
			elsif (@ASCII[$j]=="9")
			{ $quality = $quality + 24; }
			elsif (@ASCII[$j]==":")
			{ $quality = $quality + 25; }
			elsif (@ASCII[$j]==";")
			{ $quality = $quality + 26; }
			elsif (@ASCII[$j]=="<")
			{ $quality = $quality + 27; }
			elsif (@ASCII[$j]=="=")
			{ $quality = $quality + 28; }
			elsif (@ASCII[$j]==">")
			{ $quality = $quality + 29; }
			elsif (@ASCII[$j]=="?")
			{ $quality = $quality + 30; }
			elsif (@ASCII[$j]=="@")
			{ $quality = $quality + 31; }
			elsif (@ASCII[$j]=="A")
			{ $quality = $quality + 32; }
			elsif (@ASCII[$j]=="B")
			{ $quality = $quality + 33; }
			elsif (@ASCII[$j]=="C")
			{ $quality = $quality + 34; }
			elsif (@ASCII[$j]=="D")
			{ $quality = $quality + 35; }
			elsif (@ASCII[$j]=="E")
			{ $quality = $quality + 36; }
			elsif (@ASCII[$j]=="F")
			{ $quality = $quality + 37; }
			elsif (@ASCII[$j]=="G")
			{ $quality = $quality + 38; }
			elsif (@ASCII[$j]=="H")
			{ $quality = $quality + 39; }
			elsif (@ASCII[$j]=="I")
			{ $quality = $quality + 40; }
			elsif (@ASCII[$j]=="J")
			{ $quality = $quality + 41; }
	}
	return $quality;
}