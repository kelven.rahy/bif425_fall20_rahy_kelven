#!/bin/bash


Pipeline:

FastQC/fastqc java -version
# FastQC v0.11.9

time FastQC/fastqc --outdir=./ /mnt/gkhazen/NGS-Fall2020/FinalProject/392_1.fastq.gz # 12/12/2020
# real    7m36.702s
# user    7m34.092s
# sys     0m8.726s

time FastQC/fastqc --outdir=./ /mnt/gkhazen/NGS-Fall2020/FinalProject/392_2.fastq.gz # 12/12/2020
# real    7m18.155s
# user    7m14.786s
# sys     0m7.285s

Trimmomatic-0.39/trimmomatic-0.39.jar java -version
# version 0.39

time java -jar Trimmomatic-0.39/trimmomatic-0.39.jar PE -threads 8 \-trimlog ./392.log /mnt/gkhazen/NGS-Fall2020/FinalProject/392_1.fastq.gz /mnt/gkhazen/NGS-Fall2020/FinalProject/392_2.fastq.gz ./392_trimmed_R1_paired.fastq.gz ./392_trimmed_R1_unpaired.fastq.gz ./392_trimmed_R2_paired.fastq.gz ./392_trimmed_R2_unpaired.fastq.gz ILLUMINACLIP:Trimmomatic-0.39/adapters/TruSeq3-PE-2.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 # 12/12/2020
# real    24m24.084s
# user    83m22.665s
# sys     48m56.087s

time FastQC/fastqc 392_trimmed_R1_paired.fastq.gz # 13/12/2020
# real    6m4.002s
# user    6m2.192s
# sys     0m5.234s

time FastQC/fastqc 392_trimmed_R2_paired.fastq.gz # 13/12/2020
# real    6m20.163s
# user    6m18.695s
# sys     0m5.780s

wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr8.fa.gz # 13/12/2020

time zcat chr8.fa.gz > chr8.fa # 13/12/2020
# real    0m1.413s
# user    0m1.295s
# sys     0m0.117s

time bwa index -p chr8bwaidx -a bwtsw chr8.fa # 13/12/2020
# real    2m36.302s
# user    2m35.127s
# sys     0m1.011s

time bwa mem -t 8 -R "@RG\tID:392\tSM:NA12878\tPL:illumina\tLB:lib1\tPU:HNLHYDSXX:1:GCCGGACA+TGTAAGAG" chr8bwaidx 392_trimmed_R1_paired.fastq.gz 392_trimmed_R2_paired.fastq.gz > 392_aln.sam # 14/12/2020
real    39m12.942s
user    311m40.366s
sys     2m27.058s

time samtools fixmate -O bam 392_aln.sam 392_aln.bam # 14/12/2020
# real    10m54.288s
# user    10m16.188s
# sys     0m37.574s

time gatk-4.1.9.0/gatk ValidateSamFile INPUT=392_aln.bam MODE=SUMMARY # 14/12/2020
# real    5m59.596s
# user    6m55.066s
# sys     0m9.911s

time gatk-4.1.9.0/gatk SortSam INPUT=392_aln.bam OUTPUT=392_sorted.bam SORT_ORDER=coordinate # 14/12/2020
# real    8m18.121s
# user    40m19.871s
# sys     14m28.116s

time gatk-4.1.9.0/gatk MarkDuplicates INPUT=392_sorted.bam OUTPUT=392_dedup.bam METRICS_FILE=392.metrics # 14/12/2020
# real    8m3.765s
# user    164m36.178s
# sys     9m24.948s

time gatk-4.1.9.0/gatk CreateSequenceDictionary R=chr8.fa O=chr8.dict # 14/12/2020
# real    0m7.300s
# user    0m26.497s
# sys     0m1.424s

time gatk-4.1.9.0/gatk BaseRecalibrator -I 392_dedup.bam -R chr8.fa --known-sites /mnt/NGSdata/snpdb151_All_20180418.vcf -O recal_data.table # 14/12/2020
# real    6m56.582s
# user    9m32.310s
# sys     14m38.684s

time gatk-4.1.9.0/gatk ApplyBQSR -R chr8.fa -I 392_dedup.bam --bqsr-recal-file recal_data.table -O output_recal.bam # 14/12/2020
# real    21m19.558s
# user    29m35.341s
# sys     3m44.596s

time samtools index 392_dedup.bam # 14/12/2020
# real    1m0.942s
# user    0m59.281s
# sys     0m1.618s

time gatk-4.1.9.0/gatk --java-options "-Xmx8g" HaplotypeCaller -R chr8.fa -I 392_dedup.bam -O output.g.vcf.gz -ERC GVCF # 14/12/2020
# real    96m29.058s
# user    139m3.035s
# sys     5m26.357s

time gatk-4.1.9.0/gatk --java-options "-Xmx8g" GenotypeGVCFs -R chr8.fa -V output.g.vcf.gz -O 392.vcf.gz # 15/12/2020
# real    2m42.341s
# user    4m40.299s
# sys     14m47.057s



#get the snp file https://sourceforge.net/projects/snpeff/files/snpEff_latest_core.zip/

time gunzip 392.vcf.gz #16/12/2020
# real    0m0.547s
# user    0m0.461s
# sys     0m0.085s

time java -Xmx8g -jar ./snpEff/snpEff.jar ann -v -stats 392_stats.html -csvStats 392_stats.csv hg38 392.vcf > 392_ann.vcf #16/12/2020
# real    8m13.349s
# user    16m29.632s
# sys     4m49.525s



Questions:

1. Illumina v1.9 was used

2. According to the per tile plot, no reads have bad quality (it's all blue). Also, we can see in the per base sequence quality that all the sequences have a very high qualities

3.Trimming arguments:
 - input the 2 raw files 392_1 and 392_2 which represent the pairs
 - the adapter file used is TruSeq3-PE-2.fa because other adapters such as NexteraPE-PE.fa didn't get rid of adapters but this one did.
 - '2:30:10': we allow 2 mismatches to the adapter sequence, require a score of at least 10 for the alignment between adapter and read, and 30 is for the palindrome clip threshold.
 - Trailing/Leading 3: Specifies the minimum quality required to keep a leading 5’ or trailing 3’ base (here minimum Phred score of 3)
 - SLIDINGWINDOW: Window size of 4 minimum mean quality in window 15
 - MINLEN: discard sequences that are smaller than 36 base pairs after the other trimming operations. 

4. Validation trimming plots:
392_trimmed_R1_paired_fastqc.html
392_trimmed_R2_paired_fastqc.html
We can see in the html that the adapters decreased a lot.

5. Length of remaining reads after trimming:
36-151

6. Max average phred score:
time perl Functions.pl max_quality 392_trimmed_R1_paired.fastq # 15/12/2020
Max average phred score: 37
# real    22m58.591s
# user    22m52.643s
# sys     0m5.020s
time perl Functions.pl max_quality 392_trimmed_R2_paired.fastq # 15/12/2020
Max average phred score: 37
# real    23m1.695s
# user    22m57.070s
# sys     0m3.693s

7. IDs of the reads with the maximum average phred score
time perl Functions.pl max_quality_IDs 392_trimmed_R1_paired.fastq 37 # 15/12/2020
# real    30m57.144s
# user    30m50.598s
# sys     0m5.282s
See the results in file "num7_392_trimmed_R1_paired.fastq.txt"

time perl Functions.pl max_quality_IDs 392_trimmed_R2_paired.fastq 37
# real    31m16.097s
# user    31m9.002s
# sys     0m5.821s
See the results in file "num7_392_trimmed_R2_paired.fastq.txt"

8. IDs of the 10 shortest reads:
time perl Functions.pl min_10_reads 392_trimmed_R1_paired.fastq # 15/12/2020
# real    1m20.442s
# user    1m16.180s
# sys     0m4.206s
See the results in file "num8_392_trimmed_R1_paired.fastq.txt"

time perl Functions.pl min_10_reads 392_trimmed_R2_paired.fastq # 15/12/2020
# real    1m21.438s
# user    1m17.119s
# sys     0m4.263s
See the results in file "num8_392_trimmed_R2_paired.fastq.txt"

9. Percentage and number of reads mapped:
time samtools view -cF 0x4 392_aln.sam # 15/12/2020
7298753 # number of reads mapped
# real    1m51.109s
# user    1m31.089s
# sys     0m15.767s

time grep -v "^@" 392_aln.sam | wc -l # 15/12/2020
answer: 62045119 # total number of reads in the sam file
# real    0m26.223s
# user    0m9.580s
# sys     0m31.126s

percentage = 7298753/62045119 * 100 = 11.763%

10. Number of reads without a pair complement
time perl Functions.pl single_reads 392_aln.sam # 16/12/2020
# real    3m11.067s
# user    3m1.800s
# sys     0m9.138s
answer: Number of reads without a pair complement: 54746366
OR
time awk '{ print $7 }' 392_aln.sam | grep  "*" | wc -l
# real    0m48.349s
# user    0m45.120s
# sys     0m10.004s
answer: 54746366


11. Compute the number of reads without any Insertion or Deletion
time perl Functions.pl no_ins_del 392_aln.sam
# real    3m29.506s
# user    3m20.380s
# sys     0m8.984s
answer: The number of reads without any insertion or deletion is 6240085

OR

time awk '{ print $6 }' 392_aln.sam | grep -v "[ID*]" | wc -l
# real    0m46.018s
# user    0m40.001s
# sys     0m10.517s
answer: 6240085

12. Average Mapping score/quality for the mapped reads:
time perl Functions.pl avg_map_qual 392_aln.sam
# real    3m25.523s
# user    3m14.393s
# sys     0m10.992s
answer: The average mapping quality of the reads is: 37.9868175241571

13. Number of Duplicates:
samtools view -cf 0x400 392_dedup.bam
on initial bam file: time samtools view -cf 0x400 392_aln.bam
answer: 0
# real    0m49.554s
# user    0m46.971s
# sys     0m2.150s
on dedup file: time samtools view -cf 0x400 392_dedup.bam
answer: 1162982
# real    1m0.826s
# user    0m59.279s
# sys     0m1.506s

14. Number of supplementary and secondary reads:
Number of supplementary reads:
time samtools view -cf 0x800 392_aln.bam
# real    0m47.406s
# user    0m46.064s
# sys     0m1.310s
answer: 645699

Number of secondary reads:
time samtools view -cf 0x100 392_aln.bam
# real    0m46.735s
# user    0m45.680s
# sys     0m1.023s
answer: 0

15. Total Number of variants, number of SNPs and INDELs:
# From the HTML we can get the following information:
Number of variants: 
Number of SNPs: 348,617
Number of insertions: 12,528
Number of deletions: 15,534

16. Number of Homozygotes wild type, Heterozygotes, Homozygotes mutant:
time perl Functions.pl variants_info 392_ann.vcf
# real    0m1.769s
# user    0m1.702s
# sys     0m0.066s

answer:
Number of homozygotes wild type: 0
Number of homozygotes mutant: 180316
Number of heterozygotes: 193736