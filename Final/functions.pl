#!/usr/bin/perl
use List::Util qw(max);

if ($ARGV[0] eq "max_quality")
{
	max_quality($ARGV[1]);
}
elsif ($ARGV[0] eq "max_quality_IDs")
{
	max_quality_IDs($ARGV[1], $ARGV[2]);
}
elsif($ARGV[0] eq "min_10_reads")
{
	min_10_reads($ARGV[1]);
}
elsif($ARGV[0] eq "single_reads")
{
	single_reads($ARGV[1]);
}
elsif($ARGV[0] eq "no_ins_del")
{
	no_ins_del($ARGV[1]);
}
elsif($ARGV[0] eq "avg_map_qual")
{
	avg_map_qual($ARGV[1]);
}
elsif($ARGV[0] eq "variants_info")
{
	variants_info($ARGV[1]);
}
else
{
	print "Input format incorrect. Please enter your command in the following way: perl functions.pl [method name] [arguments]";
}

sub max_quality
{
	$file=@_[0];
	open(my $fh, '<', $file) or die "Cannot open '$file': $!\n";
	$i=1;
	$max_quality=0;
	while (my $row = <$fh>)
	{
	 	chomp $row;
 		if($i==4)
 		{
 			$sum=0;
			for ($k=0; $k<length($row); $k++)
			{
				$num = ord(substr($row, $k, 1));
				$sum = $sum+$num;
			}
			$quality=$sum/length($row) - 33;

 			if ($max_quality<$quality)
 			{
 				$max_quality=$quality;
 			}
 			$i=0;
 		}
 		$i++;
 	}
 	print "Max average phred score: " . "$max_quality\n"; 
 	close $fh;
}


sub max_quality_IDs
{
	$file=@_[0];
	$max_quality=@_[1];
	open(my $fh, '<', $file) or die "Cannot open '$file': $!\n";
	$new_file_name= "num7_" . $file;
	open(my $out, '>', "$new_file_name.txt") or die "Can't write new file: $!";
	print $out "The reads with the maximum quality of $max_quality are:\n";
	$i=1;
	while (my $row = <$fh>)
	{
	 	chomp $row;
 		if ($i==1) # if we're at the first row (header), save the header for later
 		{
 			$read_name=$row;
 		}
 		elsif($i==4)
 		{
 			$sum=0;
			@ASCII=split(//, $row);
			for ($k=0; $k<length($row); $k++)
			{
				$num = ord(@ASCII[$k]);
				$sum = $sum+$num;
			}
			$quality=$sum/length($row) - 33;

 			if ($max_quality==$quality)
 			{ 				
 				print $out $read_name . "\n";
 			}
 			$i=0;
 		}
 		$i++;
 	}
 	
 	close $out;
 	close $fh;
}


sub min_10_reads
{
	$file=@_[0];
	open(my $fh, '<', $file) or die "Cannot open '$file': $!\n";
	$new_file_name= "num8_" . $file;
	open(my $out, '>', "$new_file_name.txt") or die "Can't write new file: $!";

	$i=1;
	@min_10 = (102) x 10;
	@ids=("") x 10;
	while (my $row = <$fh>)
	{
	 	chomp $row;
 		if ($i==1)
 		{
 			$read_name=$row;
 		}
 		elsif($i==2) # when we're on the second line
 		{
 			$length = length($row);
 			if ($length<max(@min_10))
 			{
 				for($j=0; $j<10; $j++)
 				{
 					if (@min_10[$j] == max(@min_10)){ # search for the maximum value and replace it with the length 
 						@min_10[$j] = $length;
 						@ids[$j]=$read_name;
 						$j=10; 
 					}
 				}
 			}
 		}
 		$i++;
 		if ($i==5){
 			$i=1;
 		}
 	}
 	print $out "The read IDs of the 10 shortest reads are:\n";
 	foreach(@ids)
 	{
 		print $out $_ . "\n";
 	}
 	close $out;
 	close $fh;
}

sub single_reads
{ # returns the number of reads without a pair
	$file = @_[0];
	$count =0;
	open( $fh, '<', $file) or die "$file': $!\n";
	while ($row = <$fh>)
	{
		chomp $row;
		@array=split(/\t/, $row);
		$have_read=@array[6];
		if ($have_read eq "*")
		{
			$count++;
		}
	}
	print "Number of reads without a pair complement: " . $count;
	close $fh;
} # or awk '{ print $7 }' hi.txt | grep  "*" | wc -l

sub no_ins_del
{ # returns the number of reads without any Insertion or Deletion
	$file = @_[0];
	$count = 0;
	open( $fh, '<', $file) or die "$file': $!\n";
	while ($row = <$fh>)
	{
		chomp $row;
		@array=split(/\t/, $row);
		$cigar= @array[5];
		if ($cigar ne "*") # if it's a *, then the read is unmapped, so we don't want to count it at all
		{
		$has_ins_del=0; # 0 means it has no insertions/deletions; if it changes to 1, then we have an insertion/deletion
		for ($k=0; $k<length($cigar); $k++)
		{
			$char = substr($cigar, $k, 1);
			if ($char eq "I" || $char eq "D") # if we have either an insertion or deletion
			{
				$has_ins_del=1;
			}
		}
		if ($has_ins_del==0 ) # if we don't have an insertion/deletion
		{
			$count++;
		}
	}
	}
	print "The number of reads without any insertion or deletion is " . $count . "\n";
	close $fh;
} # or awk '{ print $6 }' hi.txt | grep -v "[ID*]" | wc -l

sub avg_map_qual
{ # returns average mapping quality for the mapped reads
	$file = @_[0];

	$count=0;
	open( $fh, '<', $file) or die "$file': $!\n";
	$sum=0;
	while ($row = <$fh>)
	{
		if ($row =~/^@/)
		{
			next;
		}
		
		chomp $row;
		@array=split(/\t/, $row);
		$quality=@array[4];
		if ($quality!=0) # do the following only if the read is mapped
		{
		$sum=$sum+$quality;
		$count++;
		}
	}
	$avg=$sum/$count;
	print "The average mapping quality of the reads is: $avg\n";
	close $fh;	
}

sub variants_info
{
	$file = @_[0];

	open(my $fh, '<', $file) or die "$file': $!\n";
	$homo_wt=0;
	$homo_mut=0;
	$hetero=0;
	while ($row = <$fh>)
	{
		chomp $row;
		if (!($row=~/^#/))
		{
		@array=split(/\t/, $row);
		$NA12878 = @array[9]; # NA12878 is the name of the column in the file
		for ($j=0; $j<length($NA12878); $j++)
		{
			if (substr($NA12878, $j,1) eq "|" || substr($NA12878, $j,1) eq "/")
			{
				$before= substr($NA12878, $j-1,1);
				$after= substr($NA12878, $j+1,1);
				if ($before eq $after)
				{
					if ($before eq "0") # if both of them are 0
					{
						$homo_wt++;
					}
					else # if both of them are not 0 but still the same, they're mutant
					{
						$homo_mut++;
					}
				}
				else # if they're not the same before and after the | or /
				{
					$hetero++;
				}
				$j=length($NA12878); # to stop the for loop
			}
		}
	}
	}
	print "Number of homozygotes wild type: " . $homo_wt . "\n";
	print "Number of homozygotes mutant: " . $homo_mut . "\n";
	print "Number of heterozygotes: " . $hetero . "\n";
	close $fh;
}